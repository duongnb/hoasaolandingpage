var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CustomerSchema = new Schema({
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  email: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  phone: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  shop: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  product: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  channel: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
  reason: {
    type: String,
    default: '',
    trim: true,
    required: 'Title cannot be blank'
  },
});

module.exports = mongoose.model('Customer', CustomerSchema);
