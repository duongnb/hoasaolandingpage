var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CommonSchema = new Schema({
  lateUpdate: {
    type: Date
  },
  latestUpdate: {
    type: Date
  }
});

module.exports = mongoose.model('Common', CommonSchema);
