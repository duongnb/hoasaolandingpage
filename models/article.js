var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  title: String,
  day: Date,
  category: String,
  description: String,
  content: String,
  tag: [String]
});

module.exports = mongoose.model('Article', ArticleSchema);
