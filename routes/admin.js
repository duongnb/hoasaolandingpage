var express = require('express');
var router = express.Router();
var multer = require('multer');
var Common = require('../models/common');
var Article = require('../models/article');
var Customer = require('../models/customer');
var upload = multer({ dest: 'uploads/' });
var uploadImg = multer({ dest: 'public/img/' });
var async = require('async');


var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

/* GET home page. */

//////////////////////////////////
// process Upload data by file  //
//////////////////////////////////

router.get('/getAllCustomers', function (req, res, next) {
  Customer.find({}, function (err, customers) {
    if (err) {
      res.status(500).send('Internal server error');
    } else {
      res.status(200).send(customers);
      console.log(customers);
    }
  })
})


/////////////////////////////////////////////////
// process articles (list, add, edit, remove)  //
/////////////////////////////////////////////////

router.get('/allArticles', function (req, res, next) {
  Article.find({}, function (err, article) {
    res.send(article);
  })
});

router.post('/addArticle', function (req, res, next) {
  console.log("adding article");
  var data = req.body;
  data.day = new Date();
  Article.collection.insert(data, function (err, article) {
    if (err) {
      console.log("err");
      res.sendStatus(500);
    } else {
      res.send(article.insertedIds[0]);
    }
  });
});

router.post('/uploadImg', uploadImg.single('imgFile'), function (req, res, next) {
  console.log("hello");
  res.send("hello");
});

router.post('/editArticle', function (req, res, next) {
  var data = req.body;
  data.day = new Date();
  Article.update({ _id: data._id }, data, function (err, article) {
    res.sendStatus(200);
  });
});

router.post('/deleteArticles', function (req, res, next) {
  Article.remove({ _id: req.body._id }, function (err, removed) {
    res.send(removed);
  });
});

router.post('/removeChecked', function (req, res, next) {
  async.each(req.body, function (item, callback) {
    Article.remove({ _id: item._id }, function (err, removed) {
      callback();
    });
  }, function (err) {
    res.sendStatus(200);
  })
});

module.exports = router;
