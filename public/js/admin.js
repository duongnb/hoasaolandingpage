var app = angular.module('admin', ['ui.bootstrap', 'ngFileUpload', 'ngRoute', 'ngAnimate', 'colorpicker.module', 'wysiwyg.module', 'ngTagsInput']);

angular.module('admin').config(function ($routeProvider) {
  $routeProvider
    .when('/news', {
      controller: 'ArticlesManagementController',
      templateUrl: '/html/admin/articles.html'
    }).when('/customers', {
      controller: 'CustomerManagementController',
      templateUrl: '/html/admin/customers.html'
    }).otherwise({
      redirectTo: '/news'
    });
});

app.service('CommonService', ['$http', function ($http) {
  return {
    getAllArticles: function (cb) {
      $http({
        method: 'GET',
        url: '/admin/allArticles'
      }).then(function successCallback(response) {
        cb(null, response.data);
      }, function errorCallback(response) {});
    },
    deleteArticles: function (data, cb) {
      $http.post('/admin/deleteArticles', data).then(function (data) {
        cb(null, data);
      }, function (err) {
        cb(err);
      });
    },
    getAllCustomers: function (cb) {
      $http({
        method: 'GET',
        url: '/admin/getAllCustomers'
      }).then(function successCallback(response) {
        cb(null, response.data);
      }, function errorCallback(response) {});
    }
  };
}]);

app.controller('CustomerManagementController', ['$scope', 'CommonService', '$uibModal', '$http', function ($scope, CommonService, $uibModal, $http) {
  CommonService.getAllCustomers(function(err, customers) {
    if (err) {
      console.log(err);
    } else {
      console.log(customers[0]);
      $scope.customers = customers;
    }
  })
}]);

app.controller('ArticlesManagementController', ['$scope', 'CommonService', '$uibModal', '$http', function ($scope, CommonService, $uibModal, $http) {
  function loadArticles() {
    CommonService.getAllArticles(function (err, articles) {
      $scope.articlesList = articles;
      $scope.showArticlesList = $scope.articlesList;
    });
  }

  loadArticles();

  $scope.showMarket = function () {
    $scope.hideFilter = $scope.showMarketChecked;
    if ($scope.showMarketChecked) {
      $scope.showArticlesList = [];
      angular.forEach($scope.articlesList, function (item) {
        if (item.tag == "Nga") {
          $scope.showArticlesList.push(item);
        }
      });
    } else {
      $scope.showArticlesList = $scope.articlesList;
    }
  }

  $scope.open = function (flag, item) {
    if (flag) {
      $scope.isCreate = true;
    } else {
      $scope.isCreate = false;
    }
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'myModalContent.html',
      controller: 'EditModalCtrl',
      size: 'lg',
      resolve: {
        isCreate: function () {
          return $scope.isCreate;
        },
        item: function () {
          return item;
        }
      }
    });

    modalInstance.result.then(function () { loadArticles(); }, function () {
      loadArticles();
    });
  };

  $scope.checkAll = function () {
    if ($scope.selectedAll) {
      $scope.selectedAll = true;
    } else {
      $scope.selectedAll = false;
    }
    angular.forEach($scope.articles, function (item) {
      item.Selected = $scope.selectedAll;
    });

  };

  $scope.edit = function (item) {
    $scope.open(false, item);
  }

  $scope.filter = function () {
    $scope.showArticlesList = [];
    if (typeof ($scope.filterTag) === 'undefined' || $scope.filterTag === '') {
      $scope.showArticlesList = $scope.articlesList;
    } else {
      angular.forEach($scope.articlesList, function (item) {
        if (item.tag == $scope.filterTag) {
          $scope.showArticlesList.push(item);
        }
      });
    }
  }

  $scope.delete = function (_id) {
    $http.post('/admin/deleteArticles', { _id: _id }).then(function (data) {
      loadArticles();
    }, function (err) {});
  }

  $scope.removeChecked = function () {
    removeList = [];
    angular.forEach($scope.showArticlesList, function (item) {
      if (item.Selected) {
        removeList.push({ _id: item._id });
      }
    });
    $http.post('/admin/removeChecked', removeList).then(function (data) {
      loadArticles();
      $scope.selectedAll = false;
    }, function (err) {});
  }

  $scope.chkAll = function () {
    angular.forEach($scope.showArticlesList, function (item) {
      item.Selected = $scope.selectedAll;
    });
  };

  $scope.checkState = function () {
    $scope.selectedAll = true;
    angular.forEach($scope.showArticlesList, function (item) {
      if (!item.Selected) {
        $scope.selectedAll = false;
      }
    });
  }

}]);

app.controller('EditModalCtrl', ['$scope', '$uibModalInstance', '$http', 'isCreate', 'item', 'Upload', function ($scope, $uibModalInstance, $http, isCreate, item, Upload) {

  if (!isCreate) {
    $scope.data = item;
  }

  $scope.data

  if (!$scope.data) {
    $scope.selectedCategory = "Select a category ";
  } else {
    $scope.selectedCategory = $scope.data.category;
  }

  $scope.categories = ["Market Summary", "Macroeconomics News", "Market News", "Coporation News", "International News", "Analysis Report", "Document", "Internal News"];
  $scope.select = function (categoryChoice) {
    $scope.selectedCategory = categoryChoice;
    $scope.data.category = categoryChoice;
  }

  $scope.disabled = false;
  $scope.isCreate = isCreate;
  $scope.menu = [
    ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
    ['format-block'],
    ['font'],
    ['font-size'],
    ['font-color', 'hilite-color'],
    ['remove-format'],
    ['ordered-list', 'unordered-list', 'outdent', 'indent'],
    ['left-justify', 'center-justify', 'right-justify'],
    ['code', 'quote', 'paragraph'],
    ['link', 'image'],
    ['css-class']
  ];

  $scope.setDisabled = function () {
    $scope.disabled = !$scope.disabled;
  };

  $scope.save = function () {
    var tags = [];
    for (var i = 0; i < $scope.data.tag.length; i++) {
      tags.push($scope.data.tag[i].text);
    }
    $scope.data.tag = tags;
    if (isCreate) {
      $http.post('/admin/addArticle', $scope.data).then(function (data) {
        Upload.upload({
          url: "/admin/uploadImg",
          data: {
            imgFile: $scope.imgFile,
            articleId: data.data
          }
        }).then(function (res) {
          alert('Import img Successfully');
          // $uibModalInstance.close();
        }, function (err) {
          console.log(err);
        });
      }, function (err) {
        console.log(err);
      });
    } else {
      $http.post('/admin/editArticle', $scope.data).then(function (data) { $uibModalInstance.close(); }, function (err) {});
    }
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);
