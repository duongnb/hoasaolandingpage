angular.module('index').service('ArticlesService', function($http) {
  return {
    signalInfo: function(ticker, cb) {
      $http.get('/signal/' + ticker).then(function(result) {
        cb(result);
      });
    },
    // signalHistory: function(ticker, cb) {
    //   $http.get('/signalHistory/' + ticker).then(function(result) {
    //     cb(result);
    //   });
    // },
    // chartService: function(ticker, cb) {
    //   $http.get('/eods/' + ticker).then(function(result) {
    //     cb(result);
    //   });
    // },
    articlesService: function(ticker, cb) {
      $http.get('/articles/' + ticker).then(function(result) {
        cb(result);
      });
    },
    articleDetailService: function(_id, cb) {
      $http.get('/articleDetail/' + _id).then(function(result) {
        cb(result);
      });
    }
  };
});

angular.module('index').controller('ArticleController', function($scope, $routeParams, $http, ArticlesService) {

  $scope.articleId = $routeParams.articleId;

  ArticlesService.articleDetailService($scope.articleId, function(result) {
    $scope.articleDetail = result.data[0];
    $scope.tags = result.data[0].tag;
    $scope.relateTicker = result.data[0].tag[0]; // hard code

    ArticlesService.signalInfo($scope.relateTicker, function(result) {
      var data = result.data;
      $scope.item = data;
      if (data.signal.priceChange > 0) {
        $scope.priceChangeState = "up";
      } else if (data.signal.priceChange < 0) {
        $scope.priceChangeState = "down";
      } else {
        $scope.priceChangeState = "equal";
      }
    });

    ArticlesService.articlesService($scope.relateTicker, function(result) {
      $scope.articles = result.data;
    });

  });

});
