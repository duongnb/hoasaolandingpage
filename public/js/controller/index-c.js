angular.module('index').service('IndexService', function ($http) {
  return {
    register: function (customer, cb) {
      $http.post('/register', customer).then(function (data) {
        cb(null, data);
      }, function (err) {
        cb(err);
      });
    }
  };
});

angular.module('index').controller('IndexController', function ($scope, IndexService) {
  $scope.customer = {};
  $scope.channels = ['Facebook/Instagram', 'Website', 'Forum', 'Khac'];
  $scope.reasons = ['Tim kiem tren mang', 'Mang xa hoi', 'Quang cao', 'Email', 'To roi/to gap', 'Ban be, nguoi than', 'Khac'];
  $scope.register = function () {
    IndexService.register($scope.customer, function (err, data) {
      console.log(data);
    });
  }
});
