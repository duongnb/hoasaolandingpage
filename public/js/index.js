angular.module('index', ['ngRoute', 'ngSanitize', 'ngMaterial']);

angular.module('index').config(function ($routeProvider, $mdThemingProvider) {
  $routeProvider
    .when('/', {
      controller: 'IndexController',
      templateUrl: '/html/index/index.html'
    }).when('/articles/:articleId', {
      controller: 'ArticleController',
      templateUrl: '/html/index/articles.html'
    })
    .otherwise({
      redirectTo: '/'
    });

  $mdThemingProvider.theme('dark-grey').backgroundPalette('grey').dark();
  $mdThemingProvider.theme('dark-orange').backgroundPalette('orange').dark();
  $mdThemingProvider.theme('dark-purple').backgroundPalette('deep-purple').dark();
  $mdThemingProvider.theme('dark-blue').backgroundPalette('blue').dark();
});
